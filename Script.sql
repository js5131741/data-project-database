-- 1. Number of matches played per year for all the years in IPL.

select  season, count(id) as matches_played  
from matches m 
group by season
order by season;

-- 2.Number of matches won per team per year in IPL.

select  season, winner, count(winner) 
from matches m 
where length(winner) > 0
group by winner, season
order by season;


-- 3.Extra runs conceded per team in the year 2016

select d.bowling_team , sum(extra_runs)
from matches m 
join	deliveries d 
on m.id = d.match_id 
where m.season = 2016
group by d.bowling_team; 

-- 4.Top 10 economical bowlers in the year 2015

select d.bowler, round((sum(total_runs)-sum(legbye_runs)-sum(bye_runs))/round(SUM(case
        when wide_runs = 0 and noball_runs = 0 
        then 1
        else 0
    end)/6.0,2),2) as economy
from matches m 
join deliveries d 
on m.id = d.match_id 
where m.season = 2015
group by d.bowler
order by economy asc
limit 10;

-- 5.Find the number of times each team won the toss and also won the match

select winner, count(winner) 
from matches m 
where winner = toss_winner 
group by winner;

-- 6.Find a player who has won the highest number of Player of the Match awards for each season

select season, player_of_match, player_count
from (
  select
    season,
    player_of_match,
    count(player_of_match) as player_count,
    rank() over (partition by season order by count(player_of_match) desc) as awards_rank
  from matches
  group by season, player_of_match
) as subquery
where awards_rank = 1;

-- 7.Find the strike rate of a batsman for each season

select m.season, d.batsman, round(sum(d.batsman_runs)*100.0/count(d.ball),2)
from deliveries d 
join matches m
on m.id = d.match_id 
where d.batsman = 'V Kohli' and d.wide_runs  = 0 and d.noball_runs = 0
group by m.season, d.batsman;

-- 8.Find the highest number of times one player has been dismissed by another player


SELECT player_dismissed, bowler, count(*) as number_of_times
FROM deliveries d 
where length(player_dismissed) > 0
group by player_dismissed, bowler 
order by number_of_times desc
limit 1;


-- 9.Find the bowler with the best economy in super overs

select d.bowler, round((sum(total_runs)-sum(legbye_runs)-sum(bye_runs))/round(SUM(case
        when wide_runs = 0 and noball_runs = 0 
        then 1
        else 0
    end)/6.0,2),2) as economy_rate
from deliveries d 
where d.is_super_over  = 1
group by bowler 
order by economy_rate
limit 1;
