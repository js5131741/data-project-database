### SQL IPL Project

Follow the below steps after setting up postgres and make sure it is running in you computer.

#### 1. Git clone and change to the directory

```git clone https://gitlab.com/js5131741/data-project-database.git```
```cd data-project-database```

#### 2. Load CSV

* Go to terminal and type the command to create tables. 
```psql -U postgres -d postgres -a -f load_csv.sql```
* Now, acess the psql client.
```psql -U postgres```
* Run the below queries in psql client to load the data into the table.
```\copy "deliveries" FROM './Data/deliveries.csv' WITH CSV HEADER DELIMITER ',';```
```\copy "matches" FROM './Data/matches.csv' WITH CSV HEADER DELIMITER ',';```

#### 3. Run the Script.sql
* Execute the sql script 'Script.sql' by running the following command in the sql client.
```\i Script.sql```
* This script will dislay the results for all the IPL assignment questions.
